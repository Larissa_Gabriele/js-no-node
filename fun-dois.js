
// function retornaBomDiaParaAlguem(quem) {
//     console.log("Bom dia, " + quem);
// }

// retornaBomDiaParaAlguem("Dutra");

const retornaBomDiaParaAlguem = quem => {
    console.log("Bom dia, " + quem);
} 

const retornaBomDiaParaAlguem2 = quem => "Bom dia, " + quem;

const msg = retornaBomDiaParaAlguem2("Manoel");
console.log(msg);